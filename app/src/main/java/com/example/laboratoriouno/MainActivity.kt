package com.example.laboratoriouno

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnProcesar.setOnClickListener {
            val Edad = edtEdad.text.toString()
            var Resultado = ""
            if(!Edad.isEmpty()){
                if (Edad.toInt() < 18) {
                    Resultado = "Eres Menor de Edad"
                } else {
                    Resultado = "Eres Mayor de Edad"
                }
            } else{
                Resultado = "Debes ingresar una edad valida"
        }
            tvResultado.text = "$Resultado"

        }


    }
}